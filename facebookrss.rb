require 'rss'
require 'open-uri'

rss = open('http://www.facebook.com/feeds/api_messages.php'){|file|
  RSS::Parser.parse(file.read)
}
rss.items.each{|item|
  p 'title:  ' + item.title.content
  p 'link    ' + item.link.href
  p 'published ' + item.published.content.strftime("%Y-%m-%d %H:%M:%S")
}
