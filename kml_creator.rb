# -*- coding: utf-8 -*-
class KmlCreator
  require 'kml'
  def create(input_dir_path, output_path)
    kml = KMLFile.new
    latitudes_list = []
    Dir::glob(input_dir_path).sort.each do |name|
      latitudes_list += latitudes_list(name)
    end
    document = create_document
    latitudes_list.each_with_index do |latitudes, idx|
      document.features << create_placemark(latitudes, idx)
    end
    kml.objects << document
    File.new("#{output_path}/kml_#{Time.now.strftime('%Y%m%d%H%M')}.kml", "w").write(kml.render)
  end

  private
  def create_document
    KML::Document.new(
                      :name => 'GPS to Kml',
                      :description => 'gpsロガーの記録をkmlに変換',
                      :styles => [
                                  KML::Style.new(
                                                 :id => 'base_style',
                                                 :line_style => KML::LineStyle.new(:color => '6f9314ff', :width => 4), # color: Transparency, Blue, Green, Red
                                                 )
                                 ] )
  end
  def latitude(gps_log)
    [gps_log.longitude, gps_log.latitude]
  end
  def latitudes_list(file_name)
    latitudes_list = []
    latitudes = []
    File.open(file_name).each_with_index do |line, i|
      gps_log = GpsLog.new(line)
      next if gps_log.header == 'Tag' || gps_log.format == "$GPRMC"
      # '-99'は電源が入った(再計測を開始するために位置情報を確認する）ところっぽい
      if gps_log.header == '-99'
        latitudes_list << latitudes unless latitudes.empty?
        latitudes = []
        next
      end
      latitudes << latitude(gps_log)
    end
    latitudes_list
  end
  def create_placemark(latitudes, idx = 0)
    KML::Placemark.new(
                       :name => "Gps to Kml-#{idx}",
                       :description => 'gpsロガーの結果をkmlに変換した',
                       :style_url => '#base_style',
                       :geometry => KML::LineString.new(
                                                        :extrude => true,
                                                        :tessellate => false,
                                                        :coordinates => latitudes
                                                        )
                       )
  end
end

class GpsLog
  # $GPGGA UTC(hhmmss.sss),Latitude,N/S,Longitude,E/W,Fix quality,Fix quality,Horizontal dilution of position,Altitude,Height of geoid,,Checksum
  # $GPRMC UTC(hhmmss.sss),A,Latitude,N/S,Longitude,E/W,Speed(knots),Course(degrees),Date(ddmmyy),,Checksum
  attr_accessor :format, :header, :latitude, :longitude
  def initialize(line)
    cells = line.split(',')
    self.header = cells[0]
    self.format = cells[1]
    case self.format
    when "$GPGGA"
      set_latitude(cells[3], cells[4])
      set_longitude(cells[5], cells[6])
    when '$GPRMC'
      set_latitude(cells[4], cells[5])
      set_longitude(cells[6], cells[7])
    end
  end

  private
  def set_latitude(data, direction)
    degree = data.to_i / 100 # 度
    self.latitude = degree + (data.to_f - degree*100) / 60 # 分表示の部分を10進数で表す
    self.latitude *= -1 if direction == 'S' # 南緯はマイナス値
  end
  def set_longitude(data, direction)
    degree = data.to_i / 100 # 度
    self.longitude = degree + (data.to_f - degree*100) / 60 # 分表示の部分を10進数で表す
    self.longitude *= -1 if direction == 'W' # 西経はマイナス値
  end
end
